#!/bin/bash

source ~/.profile
cd /server
umod install valheim -P

echo "Starting server...\n"

rm -rf steamclient.so && ln -s linux64/steamclient.so steamclient.so

chmod +x ./valheim_server.x86_64
./valheim_server.x86_64 -batchmode -nographics -name "${SERVER_NAME}" -port ${SERVER_PORT} -world "${WORLD_NAME}" -password "${SERVER_PASSWORD}" -public ${SERVER_PUBLIC} -savedir /save