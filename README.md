# UMod-Docker

docker run -it --name valheim \
    -v /tmp/save:/save \
    -v /tmp/server:/server \
    -v /tmp/plugins:/server/umod/plugins \
    -p 2456-2458:2456-2458/udp \
    -it registry.gitlab.com/norius/docker-valheim-umod/docker-valheim-umod:latest
