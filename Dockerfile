FROM debian:stable

LABEL maintainer="lou@norius.fr"

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get -y install --no-install-recommends lib32gcc1 curl openssl ca-certificates

ENV SERVER_NAME="Valheim Docker"
ENV WORLD_NAME="Dedicated"
ENV SERVER_PASSWORD="Docker"
ENV PUBLIC=1
ENV SERVER_PORT=2456

RUN curl -sSL https://umod.io/umod-develop.sh | bash /dev/stdin

WORKDIR /server/

COPY 'launcher.sh' /usr/bin/launcher.sh

RUN chmod +x /usr/bin/launcher.sh -R

ENTRYPOINT [ "/usr/bin/launcher.sh" ]